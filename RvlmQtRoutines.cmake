# This file is a part of rvlm-cmake-routines subproject.
# It contains several CMake functions to ease creation of Qt executables (and
# libraries, in future) for CMake projects. See README.md file for further
# information.
# ---
# Written by Pavel Kretov aka firegurafiku in May, 2013.
# Licensed under the terms of WTFPL of any version.

# TODO: Provide a way for user to select Qt modules.
find_package(Qt4 REQUIRED QtCore QtGui)
include(${QT_USE_FILE})

# === PRIVATE ===

function(__RVLM_TestExtension FileName ResVar)
    set(Found FALSE)
    foreach(Ext IN LISTS ARGN)
        # TODO: Make this regex case-insensitive.
        if(FileName MATCHES ".+\\.${Ext}\$")
            set(Found TRUE)
            break()
        endif()
    endforeach()
    set(${ResVar} ${Found} PARENT_SCOPE)
endfunction()

# === PUBLIC ===

function(AddQtExecutable Target)
    set(CppFiles)
    set(HppFiles)
    set(UiFiles)
    set(QrcFiles)
    set(TsFiles)
    foreach(File IN LISTS ARGN)
        __RVML_TestExtension("${File}" FileIsCpp "cpp" "cxx" "cc")
        __RVML_TestExtension("${File}" FileIsHpp "h" "hpp" "hxx" "hh")
        __RVML_TestExtension("${File}" FileIsUi  "ui")
        __RVML_TestExtension("${File}" FileIsQrc "qrc")
        __RVML_TestExtension("${File}" FileIsTs  "ts")

        if(FileIsCpp)
            list(APPEND CppFiles "${File}")
        endif()

        if(FileIsHpp)
            list(APPEND HppFiles "${File}")
        endif()

        if(FileIsUi)
            list(APPEND UiFiles  "${File}")
        endif()

        if(FileIsQrc)
            list(APPEND QrcFiles "${File}")
        endif()

        if(FileIsTs)
            list(APPEND TsFiles  "${File}")
        endif()
    endforeach()

    qt4_wrap_cpp(MocCppFiles ${HppFiles})
    qt4_wrap_ui(UiHppFiles ${UiFiles})
    qt4_add_resources(QrcCppFiles ${QrcFiles})
    qt4_add_translation(QmFiles ${TsFiles})

    if(TsFiles)
        set(TranslationUpdateTarget "${Target}TranslationsUpdate")
        set(LUpdate ${QT_LUPDATE_EXECUTABLE})
        add_custom_target(${TranslationUpdateTarget}
            COMMAND ${LUpdate} ${UiFiles} ${HppFiles} ${CppFiles} -ts ${TsFiles}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
        add_dependencies(${Target} ${TranslationUpdateTarget})
    endif()

    AddExecutable(${Target}
        ${HppFiles}
        ${UiHppFiles}
        ${CppFiles}
        ${MocCppFiles}
        ${QrcCppFiles}
        ${QmFiles})

    TargetIncludeDirectories(${Target} ${QT_INCLUDES})
    TargetIncludeDirectories(${Target} ${CMAKE_CURRENT_BINARY_DIR})
    target_link_libraries(${Target} ${QT_LIBRARIES})
endfunction()

