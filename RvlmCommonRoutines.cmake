# This file is a part of rvlm-cmake-routines subproject.
# It contains several CMake functions to ease projects dependency tracking
# of CMake projects. These functions are to intended to replace the standard
# ones, but their names are different in casing. See README.md file for
# further information.
# ---
# Written by Pavel Kretov aka firegurafiku in May, 2013.
# Licensed under the terms of WTFPL of any version.

# === PRIVATE ===

# Global variable to store target names. Target name is appended to it every
# time AddExecutable or AddLibrary gets called.
# TODO: Check if CACHE INTERNAL can be omitted.
set(__RVLM_TargetList "" CACHE INTERNAL "")

## Retrieves list of include directories for target by recursive walking on
## target's dependencies using @c INCLUDE_DIRECTORIES property values.
function(__RVLM_GetTargetDepsIncludeDirectories Target IncludeDirListVar)
    get_target_property(InterfaceLibraries ${Target} "__CR_REQUIRED_TARGETS")

    set(Accumulator)
    if (InterfaceLibraries)
        foreach(DepTarget IN LISTS InterfaceLibraries)
            # NOTE: Recursive call here.
            __RVLM_GetTargetDepsIncludeDirectories(${DepTarget} IncludeList)
            set(Accumulator ${Accumulator} ${IncludeList})
        endforeach()
    endif()

    set(TargetIncludeDirs)
    get_target_property(TargetIncludeDirs ${Target} "INCLUDE_DIRECTORIES")

    if (NOT TargetIncludeDirs)
        set(TargetIncludeDirs)
    endif()

    set(${IncludeDirListVar} ${Accumulator} ${TargetIncludeDirs} PARENT_SCOPE)
endfunction()

# === PUBLIC ===

function(AddLibrary TargetName)
    set(__RVLM_TargetList ${__RVLM_TargetList} ${TargetName} CACHE INTERNAL "")
    add_library(${ARGV})
endfunction()

function(AddExecutable TargetName)
    set(__RVLM_TargetList ${__RVLM_TargetList} ${TargetName} CACHE INTERNAL "")
    add_executable(${ARGV})
endfunction()

# TODO: Take care of keywords in $ARGN.
function(TargetLinkLibraries Target)
    set(RequiredTargets)
    get_target_property(RequiredTargets ${Target} "__CR_REQUIRED_TARGETS")

    if (NOT RequiredTargets)
        set(RequiredTargets)
    endif()

    set(RequiredTargets ${RequiredTargets} ${ARGN})
    set_target_properties(${Target} PROPERTIES
        "__CR_REQUIRED_TARGETS" "${RequiredTargets}")

    target_link_libraries(${ARGV})
endfunction()

# TODO: Take care of keywords in $ARGN.
function(TargetIncludeDirectories Target)
    set(IncludeDirectories)
    get_target_property(IncludeDirectories ${Target} "INCLUDE_DIRECTORIES")

    if(NOT IncludeDirectories)
        set(IncludeDirectories)
    endif()

    set(IncludeDirectories ${IncludeDirectories} ${ARGN})
    set_target_properties(${Target} PROPERTIES
        "INCLUDE_DIRECTORIES" "${IncludeDirectories}")
endfunction()


function(ResolveLibrariesDependencies)
    foreach(Target IN LISTS __RVLM_TargetList)
        __RVLM_GetTargetDepsIncludeDirectories(${Target} UpdatedIncludes)
        if (UpdatedIncludes)
            list(REMOVE_DUPLICATES UpdatedIncludes)
            set_target_properties(${Target}
                PROPERTIES "INCLUDE_DIRECTORIES" "${UpdatedIncludes}")
        endif()
    endforeach()
endfunction()

